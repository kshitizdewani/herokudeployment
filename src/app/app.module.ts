import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { UserService } from './services/user/user.service';
import {ServerService} from './services/server/server.service';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';    // add this
import {HttpClientJsonpModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';    // add this
import { ReactiveFormsModule} from '@angular/forms' ;
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from "./material.module";
import { httpInterceptProviders } from './providers';
import {MyMatTableModule} from "./components/mat-table/mat-table.module";



// called on every request to retrieve the token
export function jwtOptionsFactory() {
  const user = JSON.parse(localStorage.getItem('user'));
  const token = user.token;
  return {
    tokenGetter: () => token,
    whitelistedDomains: ['localhost', '127.0.0.1']
  };
}


@NgModule({
  declarations: [AppComponent,],
  entryComponents: [],
  imports: [BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientJsonpModule,
    MaterialModule,
    MyMatTableModule,
  ],
  providers: [
    httpInterceptProviders,
    StatusBar,
    SplashScreen,
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
