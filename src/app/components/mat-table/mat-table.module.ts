import { NgModule } from '@angular/core';
import { MatTableComponent} from "./mat-table.component";
import {MaterialModule} from "../../material.module";
import {CommonModule} from "@angular/common";
import {DataPropertyGetterPipePipe} from "./data-property-getter-pipe.pipe";

@NgModule({
  imports:[
      MaterialModule,
      CommonModule,
  ],
  declarations: [
  MatTableComponent,
  DataPropertyGetterPipePipe
  ],
  providers: [DataPropertyGetterPipePipe],
  exports: [
  MatTableComponent, DataPropertyGetterPipePipe
  ]
})
export class MyMatTableModule { }
