export interface Notice {
        id: number;
        sender: number;
        course: number;
        batch: number;
        student: number;
        body: string;
        attachment: any;
}

// -----------While posting a notice---------------
export interface NoticePost {
        // id: number,
        sender: number;
        course: number;
        batch: number;
        student: number;
        body: string;
        attachment: any;
}



export interface Batch {
        id: number;
        name: string;
        course: number;
}

export interface BatchTutor {
        id : number;
        name: string;
        course: string;
        courseID: number;
}

export interface Course {
        id: number;
        title: string;
        tutor: number;
}

export interface Login{
        username: string;
        password: string;
}

// ----------COMPONENT INTERFACES------------
export interface TableColumn {
  name: string; // column name
  dataKey: string; // name of key of the actual data in this column
  position?: 'right' | 'left'; // should it be right-aligned or left-aligned?
  isSortable?: boolean; // can a column be sorted?
}
