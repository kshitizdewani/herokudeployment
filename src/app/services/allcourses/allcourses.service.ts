import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AllcoursesService {
  constructor(private httpClient: HttpClient) { }

  getSyllabus(batchID: number): Observable<any[]>{
      const url = 'https://insteasy.herokuapp.com/api/syllabus/' + batchID.toString();
      console.log('hitting url ' + url);
      return this.httpClient.get<any[]>('https://insteasy.herokuapp.com/api/syllabus/' + batchID);
  }

  fetchSyllabus(batchID: number){
    return this.httpClient.get('https://insteasy.herokuapp.com/api/syllabus/' + batchID.toString())
        // ...and calling .json() on the response to return data
        .pipe(map((response: any) => response.json()));
    }

  allcourses(){
      return [
  {
  "id":1,
  "name":"Python bootcamp",
  "chapters":[
    {
      "name":"Core",
      "start":"6/11/2020",
      "end":"7/12/2020",
      "subtopics":["data types",
                  "typecasting",
                  "operators",
                  "conditions",
                  "loops",
                  "functions"
                  ]
    },
    {
      "name":"Advanced",
      "start":"10/12/2020",
      "end":"20/01/2020",
      "subtopics":[
                "classes",
                "inheritance",
                "interfaces",
                "abstraction",
                "Tkinter",
                "data file handling"
                  ]
    }
    ]
  },
  {
  "id":2,
  "name":"Java bootcamp",
  "chapters":[
    {
      "name":"Core",
      "start":"8/10/2020",
      "end":"18/12/2020",
      "subtopics":["data types",
                  "typecasting",
                  "operators",
                  "conditions",
                  "loops",
                  "functions"
                  ]
    },
    {
      "name":"Advanced",
      "start":"10/12/2020",
      "end":"20/01/2020",
      "subtopics":[
                "classes",
                "interfaces",
			          "inheritance",
			          "spring",
			          "hibernate"
                  ]
    }
    ]
  }
];
  }
}
