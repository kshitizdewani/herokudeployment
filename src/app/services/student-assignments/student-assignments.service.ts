import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StudentAssignmentsService {
  private data: any;
  constructor() { }
 getassignments(){
    this.data = {
  "student": {
    "id": 1,
    "name": "Kshitiz Dewani"
  },
  "assignments": [
    {
      "due_date": "30/12/2020",
      "submit_date": "27/12/2020",
      "course": "Java Bootcamp",
      "batch": "J1",
      "score": 41,
      "total": 50,
      "status": "Submitted"
    },
    {
      "due_date": "3/11/2020",
      "submit_date": "7/11/2020",
      "course": "Java Bootcamp",
      "batch": "J1",
      "score": 21,
      "total": 50,
      "status": "Late Submission"
    },
    {
      "due_date": "20/10/2020",
      "submit_date": "17/10/2020",
      "course": "Java Bootcamp",
      "batch": "J1",
      "score": 35,
      "total": 50,
      "status": "Submitted"
    },
    {
      "due_date": "10/08/2020",
      "submit_date": null,
      "course": "Java Bootcamp",
      "batch": "J1",
      "score": 41,
      "total": 50,
      "status": "Pending"
    }
  ]
};
    return this.data;
    // test
  }
}
