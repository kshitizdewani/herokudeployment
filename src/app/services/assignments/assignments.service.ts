import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Notice} from "../../interfaces";
import {catchError} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class AssignmentsService {

  constructor(private httpClient: HttpClient) { }
  postAssignment(data: any): Observable<any>{
    return this.httpClient.post<any>('https://insteasy.herokuapp.com/api/assignments/', data)
        .pipe(
        catchError(this.handleError)
        );
  }

  // all assignments
  getAssignments(): Observable<any>{
    const url = 'https://insteasy.herokuapp.com/api/assignments/';
    console.log('hitting url ' + url);
    return this.httpClient.get<any[]>(url);
  }

  // a particular assignment's status
  getDetails(id): Observable<any>{
    const url = 'https://insteasy.herokuapp.com/api/assignments/status-and-review/' + id.toString();
    console.log('hitting url ' + url);
    return this.httpClient.get<any[]>(url);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
      window.alert("Client error");
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      window.alert("Backend returned an unsuccessful response code.");
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
