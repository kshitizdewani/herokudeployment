import { Injectable } from '@angular/core';
import { Batch } from '../../interfaces';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BatchesService {

  constructor(private httpClient: HttpClient) { }
  getBatches(): Observable<any[]>{
    console.log('..fetching batches..');
    return this.httpClient.get<any[]>('https://insteasy.herokuapp.com/api/batches/');
  }
}
