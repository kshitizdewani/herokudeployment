import { Injectable } from '@angular/core';
import { BatchTutor } from '../../interfaces';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class BatchesCoursesService {

  constructor(private httpClient: HttpClient) {}

  get(): Observable<any[]>{
    console.log('..fetching batches..');
    return this.httpClient.get<any[]>('http://127.0.0.1:8000/api/batches');
    // test
  }
}
