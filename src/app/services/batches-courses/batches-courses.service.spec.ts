import { TestBed } from '@angular/core/testing';

import { BatchesCoursesService } from './batches-courses.service';

describe('BatchesCoursesService', () => {
  let service: BatchesCoursesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BatchesCoursesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
