import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private httpClient: HttpClient) { }
  // all profiles of students of a batch/course
  getProfiles(data: any): Observable<any>{
        console.log('inside service, data to be sent->' + JSON.stringify(data) );
        // http://127.0.0.1:8000/api/profile/all/
        return this.httpClient.get<any>('https://insteasy.herokuapp.com/api/profile/all/' , data)
        .pipe(
        catchError(this.handleError)
        );
  }
    private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
      window.alert("Client error");
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      window.alert("Backend returned an unsuccessful response code.");
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
