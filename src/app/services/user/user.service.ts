import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ServerService } from '../server/server.service';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

const baseUrl = 'https://insteasy.herokuapp.com';
// const baseUrl = 'http://127.0.0.1:8000/';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private loggedIn = new BehaviorSubject<boolean>(false);
  private token: string;

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }


  constructor(private router: Router, private server: ServerService) {

    console.log('Auth Service');
    const userData = localStorage.getItem('user');
    if (userData) {
      console.log('Logged in from memory');
      const user = JSON.parse(userData);
      this.token = user.token;
      console.log('user ' + JSON.stringify(user));
      this.server.setLoggedIn(true, this.token);
      this.loggedIn.next(true);
    }
  }

  getToken(){
    return this.token;
  }

  login(user) {
    if (user.username !== '' && user.password !== '' ) {
      return this.server.request('POST', '/api/token/', {
        username: user.username,
        password: user.password
      }).subscribe((response: any) => {
        console.log(response);
        console.log('response.token:' + response.access);
        console.log('response.auth:' + response.auth);
        if (response.access !== undefined) {

          this.token = response.access;
          this.server.setLoggedIn(true, this.token);
          this.loggedIn.next(true);
          const userData = {
            token: this.token,
          };
          localStorage.setItem('user', JSON.stringify(userData));
          console.log('logged in as'+this.token);
          this.router.navigateByUrl('/notice-board');
        }
      });
    }
  }

  logout() {

    this.server.setLoggedIn(false);
    delete this.token;
    this.loggedIn.next(false);
    localStorage.clear();
    console.log('logged out');
    this.router.navigate(['/']);
  }
}

