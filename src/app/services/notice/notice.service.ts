import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {Notice, NoticePost} from '../../interfaces';
import {convertUpdateArguments} from "@angular/compiler/src/compiler_util/expression_converter";

@Injectable({
  providedIn: 'root'
})
export class NoticeService {
  newNotice: Observable<any>;

  constructor(  private httpClient: HttpClient) { }

  getNotices(): Observable<any[]>{
    console.log('Notice fetch request sent...');
    const notices = this.httpClient.get<any[]>('https://insteasy.herokuapp.com/api/notices/');
    console.log(notices);
    return notices;
  }

  postNotice(data: NoticePost): Observable<Notice>{
    return this.httpClient.post<Notice>('https://insteasy.herokuapp.com/api/notices/', data)
        .pipe(
        catchError(this.handleError)
        );

  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
