import { Component, OnInit } from '@angular/core';
import { AssignmentsService } from "../../services/assignments/assignments.service";

@Component({
  selector: 'app-review-assignment',
  templateUrl: './review-assignment.page.html',
  styleUrls: ['./review-assignment.page.scss'],
})
export class ReviewAssignmentPage implements OnInit {
  assignment_number : number;
  assignments = [];
  detail : any;
  private ordersTableColumns: any;
  studentwise_list = [];
  selectedCourse: any;
  selectedBatch: any;

  constructor(private _assignmentService: AssignmentsService) { }

  ngOnInit() {
    this._assignmentService.getAssignments()
        .subscribe(x => this.assignments = x);
  }

  showDetails(){
    // let item1 = array.find(i => i.id === 1);
    this._assignmentService.getDetails(this.assignment_number)
        .subscribe(x => this.detail = x);
    // this.studentwise_list = this.detail.get('studentwise_list');
    // console.log(this.studentwise_list);
  }

    // ------Table Columns----------


  initializeColumns(): void {
    this.ordersTableColumns = [
      {
        name: 'Student Name',
        dataKey: 'name',
        position: 'left',
        isSortable: false
      },
      {
        name: 'Status',
        dataKey: 'submitted',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Submit date',
        dataKey: 'submission_date',
        position: 'right',
        isSortable: false
      },
      {
        name: 'File link',
        dataKey: 'file',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Review status',
        dataKey: 'batch',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Remarks',
        dataKey: 'remarks',
        position: 'right',
        isSortable: false
      },
    ];
  }
}
