import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ReviewAssignmentPageRoutingModule } from './review-assignment-routing.module';
import {MatTableComponent} from "../../components/mat-table/mat-table.component";
import { ReviewAssignmentPage } from './review-assignment.page';
import {MaterialModule} from "../../material.module";
import {MyMatTableModule} from "../../components/mat-table/mat-table.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReviewAssignmentPageRoutingModule,
    MaterialModule,
    MyMatTableModule
  ],
  declarations: [ReviewAssignmentPage,]
})
export class ReviewAssignmentPageModule {}
