import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReviewAssignmentPage } from './review-assignment.page';

const routes: Routes = [
  {
    path: '',
    component: ReviewAssignmentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReviewAssignmentPageRoutingModule {}
