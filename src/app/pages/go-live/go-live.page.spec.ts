import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoLivePage } from './go-live.page';

describe('GoLivePage', () => {
  let component: GoLivePage;
  let fixture: ComponentFixture<GoLivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoLivePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoLivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
