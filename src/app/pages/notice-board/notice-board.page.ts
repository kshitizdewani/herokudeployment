/* tslint:disable:no-trailing-whitespace */
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NoticeService} from '../../services/notice/notice.service';
import { BatchesCoursesService } from '../../services/batches-courses/batches-courses.service';
import {Batch, Course, Notice, TableColumn, BatchTutor} from '../../interfaces';
import {BatchesService} from '../../services/batches/batches.service';
import {CoursesService} from '../../services/courses/courses.service';
import {Observable} from 'rxjs';
import {ProfileService} from "../../services/profile/profile.service";
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {MyMatTableModule} from "../../components/mat-table/mat-table.module";
import {MatAutocompleteModule} from '@angular/material/autocomplete';

@Component({
  selector: 'app-notice-board',
  templateUrl: './notice-board.page.html',
  styleUrls: ['./notice-board.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NoticeBoardPage implements OnInit {
  public notices = [];
  public batches = [];
  public courses = [];
  public textdata = '';
  public  selectedBatch = null;
  public selectedCourse = null;
  newpost: Observable<Notice>;
  ordersTableColumns: TableColumn[];
  selectedFile: File = null;
  isItemAvailable = false;
  profiles = [];
  items = []; // students of a batch/course

  constructor(private _noticeservice: NoticeService,
              private _batchservice: BatchesService,
              private _courseservices: CoursesService,
              private _batches_courses: BatchesCoursesService,
              private _profileservice: ProfileService,
              private cd: ChangeDetectorRef) {
    // console.log(this.notices);
  }

  ngOnInit() {
    this._noticeservice.getNotices()
        .subscribe(data => {
            this.notices = data;
            console.log('notice api data: ' + JSON.stringify(data));
            this.cd.detectChanges();
          }
        );
    this._batchservice.getBatches()
        .subscribe(x => this.batches = x);


    this.initializeColumns();
  }

     initializeItems(){

          var x = {};

          if (this.selectedBatch != null){
              x["batch_id"] = this.selectedBatch;
              console.log('added batch'+this.selectedCourse+this.selectedBatch);
          }
          if (this.selectedCourse != null){
              x["course_id"] = this.selectedCourse;
              console.log('added course'+this.selectedCourse+this.selectedBatch);
          }
          console.log('after updating' + JSON.stringify(x) );

         this._profileservice.getProfiles(x).subscribe(data => this.profiles = data);
         for (var profile of this.profiles) {

                 console.log(JSON.stringify([profile]));

            // console.log(profile + " -> " + this.profiles[profile].student__username);
            }

     }

     getItems(ev: any) {
         // Reset items back to all of the items
         this.initializeItems();

         // set val to the value of the searchbar
         const val = ev.target.value;

         // if the value is an empty string don't filter the items
         if (val && val.trim() !== '') {
             this.isItemAvailable = true;
             this.items = this.items.filter((item) => {
                 return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
             });
         } else {
             this.isItemAvailable = false;
         }
     }

   onFileChange(event) {
    this.selectedFile = (event.target.files[0] as File);
    console.log('got the file.');
    console.log(this.selectedFile);
  }

  createNotice() {
    console.log('..createNotice() running');
    console.log(this.selectedFile);
    this._noticeservice
    .postNotice({
        sender: 1 ,
        course: this.selectedCourse,
        batch: this.selectedBatch,
        student: null,
        body: this.textdata,
        attachment: this.selectedFile,
    })
    .subscribe(item => this.notices.push(item));
    this._noticeservice.getNotices()
    .subscribe(data => {
      this.notices = data;
      this.cd.detectChanges();
    }
    );
  }


  // ------Table Columns----------
  initializeColumns(): void {
    this.ordersTableColumns = [
      {
        name: 'Date',
        dataKey: 'date',
        position: 'left',
        isSortable: false
      },
      {
        name: 'Course',
        dataKey: 'course',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Batch',
        dataKey: 'batch',
        position: 'right',
        isSortable: false
      },
      // {
      //   name: 'Student',
      //   dataKey: 'student',
      //   position: 'right',
      //   isSortable: false
      // },
      {
        name: 'Announcement',
        dataKey: 'content',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Attachment',
        dataKey: 'attachment',
        position: 'right',
        isSortable: false
      },
    ];
  }


// ------------Text Editor-----------
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '345px',
      minHeight: '50',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'no',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
  };

}
