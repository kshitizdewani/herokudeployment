import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NoticeBoardPageRoutingModule } from './notice-board-routing.module';
import { NoticeBoardPage } from './notice-board.page';
import { MaterialModule } from "../../material.module";
import { AngularEditorModule } from '@kolkov/angular-editor';
import {MyMatTableModule} from "../../components/mat-table/mat-table.module";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoticeBoardPageRoutingModule,
    MaterialModule,
    AngularEditorModule,
    MyMatTableModule
    // AppModule
    // MatTableComponent
  ],
  exports: [],
  declarations: [NoticeBoardPage,]

})
export class NoticeBoardPageModule {}
