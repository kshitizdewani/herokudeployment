import { Component, OnInit } from '@angular/core';
import { AllcoursesService } from "../../services/allcourses/allcourses.service";
import {BatchesService} from "../../services/batches/batches.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-overview',
  templateUrl: './overview.page.html',
  styleUrls: ['./overview.page.scss'],
})
export class OverviewPage implements OnInit {
  public courses = [];
  public batches = [];
  public syllabus = [];
  public selected_batch: number;


  constructor(private _allCoursesService: AllcoursesService, private _batchesService: BatchesService) { }

  ngOnInit() {
    // if (this.selected_batch != null){
    //   this._allCoursesService.getSyllabus(this.selected_batch).subscribe(x => this.syllabus = x);
    // }
    this._batchesService.getBatches()
        .subscribe(x => this.batches = x);
    console.log(this.courses);
  }
  fetchSyllabus(batchID){
    console.log('fetchsyllabus running for batch' + this.selected_batch);
    console.log('batch id selected: ' + batchID);

    this._allCoursesService.getSyllabus(batchID).subscribe(data => this.syllabus = data );
  }


}
