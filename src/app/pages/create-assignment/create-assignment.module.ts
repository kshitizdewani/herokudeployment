import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateAssignmentPageRoutingModule } from './create-assignment-routing.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { CreateAssignmentPage } from './create-assignment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateAssignmentPageRoutingModule,
    AngularEditorModule,
  ],
  declarations: [CreateAssignmentPage]
})
export class CreateAssignmentPageModule {}
