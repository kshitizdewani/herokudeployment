import { Component, OnInit } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {BatchesService} from "../../services/batches/batches.service";
import { AssignmentsService} from "../../services/assignments/assignments.service";

@Component({
  selector: 'app-create-assignment',
  templateUrl: './create-assignment.page.html',
  styleUrls: ['./create-assignment.page.scss'],
})
export class CreateAssignmentPage implements OnInit {
  public courses = [];
  public batches = [];
  public selected_course: number;
  public selected_batch: number;
  public selected_student: number;
  points: number;
  due_date: string;
  type: string;
  textdata: any;
  attachment: File;
  constructor(private _batchesService: BatchesService, private _assignmentsService: AssignmentsService) { }
  ngOnInit() {
  this._batchesService.getBatches()
        .subscribe(x => this.batches = x);
  }

  createAssignment(){
    var recipient_type = '';
    var recipient_id:number;
    if (this.selected_course != null){
      if (this.selected_batch != null){
        if (this.selected_student != null){
          recipient_type = 'user';
        }
        else {
          recipient_type = 'batch';
          recipient_id = this.selected_batch;
        }
      }
      else{
        recipient_type = 'course';
        recipient_id = this.selected_course;
      }
    }
    var x = {
          type : this.type,
          recipient_type : recipient_type,
          recipient_id : recipient_id,
          points : this.points,
          due_date : this.due_date,
          content : this.textdata
    };
    if (this.attachment != null){
      x['attachment'] = this.attachment;
    }
    this._assignmentsService
        .postAssignment(x)
        .subscribe();
    console.log('assignment creation object sent. ' + x);
  }

  loadImageFromDevice(event) {
  this.attachment = event.target.files[0];
  console.log(this.attachment.name);
  const reader = new FileReader();
  reader.readAsArrayBuffer(this.attachment);
  reader.onload = () => {
    // get the blob of the image:
    let blob: Blob = new Blob([new Uint8Array((reader.result as ArrayBuffer))]);
    // create blobURL, such that we could use it in an image element:
    let blobURL: string = URL.createObjectURL(blob);
    console.log(blob);
    console.log(blobURL);
  };
  reader.onerror = (error) => {
    //handle errors
  };
};
// ------------Text Editor-----------
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '345px',
      minHeight: '50',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'no',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
  };

}
