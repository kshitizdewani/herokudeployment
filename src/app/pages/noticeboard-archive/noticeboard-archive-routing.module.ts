import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoticeboardArchivePage } from './noticeboard-archive.page';

const routes: Routes = [
  {
    path: '',
    component: NoticeboardArchivePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoticeboardArchivePageRoutingModule {}
