import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NoticeboardArchivePage } from './noticeboard-archive.page';

describe('NoticeboardArchivePage', () => {
  let component: NoticeboardArchivePage;
  let fixture: ComponentFixture<NoticeboardArchivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoticeboardArchivePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NoticeboardArchivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
