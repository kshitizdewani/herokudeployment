import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoticeboardArchivePageRoutingModule } from './noticeboard-archive-routing.module';
import {MatTableComponent} from "../../components/mat-table/mat-table.component";
import {DataPropertyGetterPipePipe} from "../../components/mat-table/data-property-getter-pipe.pipe";
import { NoticeboardArchivePage } from './noticeboard-archive.page';
import {MaterialModule} from "../../material.module";
import {MyMatTableModule} from "../../components/mat-table/mat-table.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoticeboardArchivePageRoutingModule,
    MaterialModule,
    MyMatTableModule
  ],
  exports: [ ],
  declarations: [NoticeboardArchivePage,]
})
export class NoticeboardArchivePageModule {}
