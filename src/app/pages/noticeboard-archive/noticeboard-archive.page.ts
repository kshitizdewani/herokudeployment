import { ChangeDetectorRef, ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Notice, TableColumn} from "../../interfaces";
import {NoticeService} from "../../services/notice/notice.service";

@Component({
  selector: 'app-noticeboard-archive',
  templateUrl: './noticeboard-archive.page.html',
  styleUrls: ['./noticeboard-archive.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NoticeboardArchivePage implements OnInit {
  public notices = [];
  newpost: Observable<Notice>;
  ordersTableColumns: TableColumn[];

  constructor(private _noticeservice: NoticeService,
              private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this._noticeservice.getNotices()
        .subscribe(data => {
            this.notices = data;
            this.cd.detectChanges();
          }
        );
    // initialize table columns
    this.initializeColumns();
  }

  // ------Table Columns----------
  initializeColumns(): void {
    this.ordersTableColumns = [
      {
        name: 'Sender',
        dataKey: 'sender',
        position: 'left',
        isSortable: false
      },
      {
        name: 'Course',
        dataKey: 'course',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Batch',
        dataKey: 'batch',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Student',
        dataKey: 'student',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Announcement',
        dataKey: 'body',
        position: 'right',
        isSortable: false
      },
    ];
  }

}
