import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentAssignmentReportPage } from './student-assignment-report.page';

const routes: Routes = [
  {
    path: '',
    component: StudentAssignmentReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentAssignmentReportPageRoutingModule {}
