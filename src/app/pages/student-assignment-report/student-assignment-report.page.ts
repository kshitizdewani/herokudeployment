import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { StudentAssignmentsService } from "../../services/student-assignments/student-assignments.service";
import {TableColumn} from "../../interfaces";
import html2canvas from "html2canvas";
import { jsPDF } from "jspdf";


@Component({
  selector: 'app-student-assignment-report',
  templateUrl: './student-assignment-report.page.html',
  styleUrls: ['./student-assignment-report.page.scss'],
})
export class StudentAssignmentReportPage implements OnInit {
  public assignments = [];
  ordersTableColumns: TableColumn[];

  constructor(
      private _assignment: StudentAssignmentsService,
      private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.assignments = this._assignment.getassignments();
  //  Initialize table Columns
    this.initializeColumns();
  }

//  ------ Export as PDf
    exportAsPDF(toPDF)
    {
        let data = document.getElementById(toPDF);
        html2canvas(data).then(canvas => {
            let h= data.offsetHeight;
            let w= data.offsetWidth;
            console.log('height:'+h);
            console.log('width:'+w);
            const contentDataURL = canvas.toDataURL('image/png')
            //Generates PDF in landscape mode
            let pdf = new jsPDF('l', 'px', 'a4');
            // let pdf = new jspdf('p', 'cm', 'a4'); Generates PDF in portrait mode
            pdf.addImage(contentDataURL, 'PNG', 0, 0, w*2, h);
            pdf.save('studentAssignmentReport.pdf');
        });
    }

    generatePDF() {
        var data = document.getElementById('toPDF');
        html2canvas(data).then(canvas => {
            var imgWidth = 208;
            var imgHeight = canvas.height * imgWidth / canvas.width;
            const contentDataURL = canvas.toDataURL('image/png')
            let pdf = new jsPDF('p', 'mm', 'a4');
            var position = 0;
            pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
            pdf.save('newPDF.pdf');
        });
    }
// -----Table Columns----
    initializeColumns(): void {
        this.ordersTableColumns = [
            {
                name: "Due date",
                dataKey: "due_date",
                position: "right",
                isSortable: false
            },
            {
                name: "Submit date",
                dataKey: "submit_date",
                position: "right",
                isSortable: false
            },
            {
                name: "Course",
                dataKey: "course",
                position: "right",
                isSortable: false
            },
            {
                name: "Batch",
                dataKey: "batch",
                position: "right",
                isSortable: false
            },
            {
                name: "Score",
                dataKey: "score",
                position: "right",
                isSortable: false
            },
            {
                name: "total score",
                dataKey: "total",
                position: "right",
                isSortable: false
            },
            {
                name: "Status",
                dataKey: "status",
                position: "right",
                isSortable: true
            },
        ];
    }

}
