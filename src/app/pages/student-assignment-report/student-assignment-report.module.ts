import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StudentAssignmentReportPageRoutingModule } from './student-assignment-report-routing.module';
import {MatTableComponent} from "../../components/mat-table/mat-table.component";
import {DataPropertyGetterPipePipe} from "../../components/mat-table/data-property-getter-pipe.pipe";
import { StudentAssignmentReportPage } from './student-assignment-report.page';
import {MaterialModule} from "../../material.module";
import {MyMatTableModule} from "../../components/mat-table/mat-table.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StudentAssignmentReportPageRoutingModule,
    MaterialModule,
    MyMatTableModule
  ],
  declarations: [StudentAssignmentReportPage,]
})
export class StudentAssignmentReportPageModule {}
