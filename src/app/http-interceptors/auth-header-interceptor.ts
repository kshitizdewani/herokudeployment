import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { UserService } from "../services/user/user.service";

@Injectable()
export class AuthHeaderInterceptor implements HttpInterceptor {
    // intercept(request: HttpRequest<any>, next: HttpHandler) {
        // if (localStorage.getItem("user")){
        //     const user = JSON.parse(localStorage.getItem('user'));
        //     const token = user.token;
        //     const authRequest = request.clone({
        //     setHeaders : { user: token }
        // });
        // console.log(UserService)
        // return next.handle(authRequest);
        // }
        // else{
        //     return next.handle(request);
        // }
        intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
            const user = JSON.parse(localStorage.getItem('user'));
            if (user && user.token) {
              request = request.clone({
                setHeaders: {
                  Authorization: `Bearer ${user.token}`
                }
              });
            }
            return next.handle(request);
            // test
        
    }
}